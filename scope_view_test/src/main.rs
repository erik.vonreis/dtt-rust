use dtt::data_source::DataSourceRef;
use dtt::data_source::nds_cache::{DataFlow, NDS2Cache};
use dtt::params::channel_params::{Channel, NDSDataType};
use dtt::{AnalysisResult, PipDuration, PipInstant, TimeDomainValue};
use dtt::scope_view::ViewSet;
use dtt::user::ResponseToUser;

fn main() {

    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(run_test());

}

async fn run_test() {
    let (mut d, mut user_rx) = dtt::init_async().await.unwrap();
    let ds: DataSourceRef = NDS2Cache::new(1<<30, "".into()).into();
    d.set_data_source(ds.clone()).unwrap();

    let span_pip = PipDuration::from_seconds(30.0);

    // let channels = vec![
    //     //Channel::new("H1:GDS-CALIB_STRAIN".into(), NDSDataType::Float64, (1<<14) as f64),
    //     Channel::new("H1:ALS-X_REFL_CTRL_OUT_DQ".into(), NDSDataType::Float64, (1<<14) as f64),
    // ];

    let channels = vec![
        "H1:ALS-X_REFL_CTRL_OUT_DQ".to_string(),
    ];

    let set = ViewSet::from_channel_names(channels);
    d.new_online_scope_view(1, span_pip, set.clone()).unwrap();

    let start_pip = ds.now();

    let mut first_start_gps = PipInstant::from_gpst_seconds(0.0);
    let mut delta = PipDuration::from_seconds(0.0);
    let mut count = 0;

    let mut state = 0;
    let n = 2;

    loop {
        let msg = match user_rx.recv().await {
            Some(m) => m,
            None => {
                println!("User input channel closed");
                break;
            }
        };

        let now = ds.now();

        if let ResponseToUser::ScopeViewResult{id, result} = msg {
            if let AnalysisResult::TimeHistoryReal{channel,
                value: TimeDomainValue::FixedStepArrayPair(tda)} = result {
                let (min, max) = tda.as_ref();
                let sp = min.start_gps_pip;
                let ep = min.end_gps_pip();
                if count == 0 {
                    first_start_gps = sp;
                    delta = now - ep;
                }
                
                let diff = now - ep - delta;
                let data_span = ep - sp;
                println!("Time = {} Delta = {} Start = {} end = {} data span = {} state = {}", (now - start_pip).to_seconds(),
                         diff.to_seconds(), (sp - first_start_gps).to_seconds(), (ep - first_start_gps).to_seconds(),
                         data_span.to_seconds(), state);
                count += 1;



                // if state == 0 && data_span.to_seconds() > 25.0 {
                //     let new_span_pip = PipDuration::from_seconds(60.0 * 60.0);
                //     d.close_scope_view(1).unwrap();
                //     d.new_online_scope_view(2, new_span_pip, set.clone()).unwrap();
                //     state = 1;
                // } else if state > 0 && state < n+1 && data_span.to_seconds() > 1100.0 {
                //     if state == n {
                //         d.close_scope_view(2).unwrap();
                //         d.new_online_scope_view(3, span_pip, set.clone()).unwrap();
                //         state = n+1;
                //     } else {
                //         state += 1;
                //     }
                // }
                
            } else {
                println!("Got result with wrong type: {}", result);
            };
        }
        else {
            println!("Got message: {}", msg);
        }
    }
}