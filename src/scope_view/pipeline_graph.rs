//! Create pipeline graph for a ScopeView

use crate::analysis::graph::analysis::{create_analysis_graph, AnalysisGraph};
use crate::analysis::graph::scheme::{SchemeEdge, SchemeGraph, SchemeNode, SchemePipelineType};
use crate::errors::DTTError;
use crate::scope_view::ScopeView;


pub fn create_pipeline_graph<'b>(view: &ScopeView) -> Result<AnalysisGraph<'b>, DTTError> {

    // Because we aren't handling functions yet, we can just hook data source to results
    // and pass it as a per-channel graph
    // When arbitrary functions are allowed on individual channels, then
    // Some more involved method will be needed.

    let mut scheme = SchemeGraph::new();

    let ds_index = scheme.add_node(
        SchemeNode::new("data_source", SchemePipelineType::DataSource));
    
    let condition_index = scheme.add_node(SchemeNode::new("conditioning", SchemePipelineType::Conditioning));

    //let splice_index = scheme.add_node(SchemeNode::new("splice", SchemePipelineType::Splice));

    let results_index = scheme.add_node(SchemeNode::new("results",
                                                        SchemePipelineType::Results));
    
    let downsample_index = scheme.add_node(SchemeNode::new("downsample", SchemePipelineType::Downsample));

    scheme.add_edge(ds_index, condition_index, SchemeEdge::new(1));
    
    // scheme.add_edge(condition_index, splice_index, SchemeEdge::new(1));
    // scheme.add_edge(splice_index, results_index, SchemeEdge::new(1));
    
    scheme.add_edge(condition_index, downsample_index, SchemeEdge::new(1));
    scheme.add_edge(downsample_index, results_index, SchemeEdge::new(1));
    
    //scheme.add_edge(condition_index, results_index, SchemeEdge::new(1));

    // get the list of channels
    

    // Convert the scheme graph to a real analysis graph
    let channels: Vec<_> = view.set.clone().into();
    let graph = create_analysis_graph(channels.as_slice(), &scheme, &SchemeGraph::new())?;

    Ok(graph)
}

