//! # handle data sources
//! such as NDS to servers, cache etc

pub mod data_source_pipeline;
pub mod data_distributor;
pub mod dummy;
pub mod nds2_direct;
pub mod random;

pub mod nds_cache;
pub mod no_data;

use std::collections::{HashMap, HashSet};
use std::fmt::{Display, Formatter};
use std::ops::Deref;
use std::sync::Arc;
use std::thread::Scope;
use nds_cache_rs::{buffer::Buffer};
use tokio::sync::mpsc;
use crate::errors::DTTError;
use crate::params::channel_params::{Channel};
use crate::params::test_params::StartTime;
use crate::run_context::RunContext;
use gps_pip::{PipInstant};
use crate::timeline::{CountSegments, Timeline};

pub (crate) type DataBlockSender = mpsc::Sender<DataBlock>;
pub (crate) type DataBlockReceiver = mpsc::Receiver<DataBlock>;

#[cfg(feature = "python")]
use pyo3::{
    pyclass,
    pymethods,
};
use crate::scope_view::ScopeView;

#[derive(PartialEq,Eq, Hash)]
pub enum DataSourceFeatures {
    // /// Supports fetch operations (get a single block of data)
    // Fetch,

    /// Supports stream operations: send multiple blocks via channel
    Stream,

    /// Send live data in perpetuity via stream
    LiveStream,
}

impl Display for DataSourceFeatures {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            DataSourceFeatures::Stream => f.write_str("Stream"),
            DataSourceFeatures::LiveStream => f.write_str("LiveStream"),
        }
    }
}

/// represents a single block of multichannel data
/// modeled after data returned from an NDS server
type DataBlock = HashMap<Channel, Vec<Buffer>>;

/// All data sources implement this trait
pub trait DataSource: Send + Sync {

    /// Stream data in chunks over a channel
    /// If end_pip is None, then live data is streamed indefinitely
    /// This is in order data meant for a single DTT test.
    fn stream_data(&self, rc: Box<RunContext>, channels: &[Channel], start_pip: PipInstant, end_pip: Option<PipInstant>)
                   -> Result<DataBlockReceiver, DTTError>;

    /// continuously send (possibly) out of order data, as with ndscope.
    fn start_scope_data(&self, rc: Box<RunContext>, view: &mut ScopeView)
                        -> Result<DataBlockReceiver, DTTError>;

    /// update an existing scope data stream with a DataBlockSender that was previously
    /// returned by a start_scope_data() call
    fn update_scope_data(&self, rc: Box<RunContext>, view: &mut ScopeView)
                         -> Result<(), DTTError>;

    /// Get a list of available features for the data source
    fn capabilities(&self) -> HashSet<DataSourceFeatures>;

    fn as_ref(&self) -> DataSourceRef;

    /// Returns true if the data source has the capabilities to complete the timeline.
    fn check_timeline_against_capabilities(&self, timeline: &Timeline) -> Result<(), HashSet<DataSourceFeatures>> {

        let mut missing_caps = HashSet::new();

        let cap = self.capabilities();
        if let StartTime::Unbound() = &timeline.start_time_pip {
            if !cap.contains(&DataSourceFeatures::LiveStream) {
                missing_caps.insert(DataSourceFeatures::LiveStream);
            }
        }
        if let CountSegments::Indefinite= &timeline.segment_count {
            if !cap.contains(&DataSourceFeatures::LiveStream) {
                missing_caps.insert(DataSourceFeatures::LiveStream);
            }
        }

        if !cap.contains(&DataSourceFeatures::Stream) {
            missing_caps.insert(DataSourceFeatures::Stream);
        }

        if missing_caps.len() > 0 {
            Err(missing_caps)
        }
        else {
            Ok(())
        }
    }

    /// Get the current time at the data source.  A good data source should override
    /// and actually query the data source.
    /// The default just gets the system time.  Just hope it's in sync!
    fn now(&self) -> PipInstant {
        
        let ns = unsafe{ crate::GPSnow_ns() } as u64;
        PipInstant::from_gpst_nanoseconds(ns)
    }
}

/// wrappers needed for python

#[derive(Clone)]
#[cfg_attr(feature = "python", pyclass(name="DataSource"))]
pub struct DataSourceRef(Arc<dyn DataSource>);

impl DataSourceRef {
    pub fn new(ds: Arc<dyn DataSource>) -> Self {
        Self(ds)
    }
}

impl From<DataSourceRef> for Arc<dyn DataSource>{
    fn from(ds: DataSourceRef) -> Self {
        ds.0
    }
}

impl <T: DataSource + 'static> From<T> for DataSourceRef {
    fn from(ds: T) -> Self {
        DataSourceRef::new(Arc::new(ds))
    }
}

impl <T: DataSource + 'static> From<Arc<T>> for DataSourceRef {
    fn from(ds: Arc<T>) -> Self {
        DataSourceRef::new(ds)
    }
}

// Implement functions that look like an implementation of DataSource but aren't
// So it can be used as a DataSource, sort of.
#[cfg_attr(feature = "python", pymethods)]
impl DataSourceRef {
    pub fn now(&self) -> PipInstant {
        self.0.now()
    }
}

impl Deref for DataSourceRef {
    type Target = dyn DataSource;

    fn deref(&self) -> &Self::Target {
        self.0.as_ref()
    }
}