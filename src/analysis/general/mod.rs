//! general pipelines are used during analysis in multiple kinds of tests.

pub mod average;
pub mod into;
pub mod maybe_into;
mod identity;