//! Average the successive values of a frequency domain function

use std::sync::Arc;
use pipelines::{PipeData, PipelineSubscriber};
use pipelines::accumulator::Accumulator;
use user_messages::UserMsgProvider;
use crate::analysis::types::{Accumulation, AccumulationStats};
use crate::analysis::types::linear::Linear;

fn average<'a, T>(rc: Box<dyn UserMsgProvider>, input: Arc<T>,
              accum: Option<Arc<T>>, n: f64) -> (Arc<T>, f64)
where
    T: Clone + Linear<'a, T> + 'static + Accumulation,
{
    match accum {
        None => {
            let output = input.as_ref().clone();
            output.set_accumulation_stats(AccumulationStats{
                n: 1.0
            });
            (Arc::new(output), 2.0)
        }
        Some(t) => {
            let a = 1.0 / n;
            let b = 1.0 - a;

            let scaled_inp = input.as_ref().clone() * (a/b);
            let backup_t = t.clone();

            // have to make sure we can actually add these
            let avg = match scaled_inp + t {
                Ok(v) => {
                    let output= v * b;   // need to normalize so the total factor is 1.
                    let output_accum = output.set_accumulation_stats(AccumulationStats{n,});
                    Arc::new(output_accum)
                },
                Err(e) => {
                    let msg = format!("Error when trying to accumulate a value: {}", e.to_string());
                    rc.user_message_handle().error(msg);
                    backup_t
                }
            };


            (avg, n+1.0)
        }

    }
}

pub async fn create<'a, T>(rc: Box<dyn UserMsgProvider>, name: impl Into<String>,
                       input: &PipelineSubscriber<T>) -> PipelineSubscriber<T>
where
    T: PipeData + Linear<'a, T> + Accumulation
{
    Accumulator::start(rc, name.into(), input, average).await
}

