//! Pipeline data types

use std::ops::{AddAssign, DivAssign, SubAssign};
use pipelines::{
    complex::c128,
};

pub mod time_domain_array;
pub mod frequency_domain_array;
pub mod linear;


/// Allows some math operations on TimeDomain array types
pub trait Scalar: Default + Copy + Clone
+ AddAssign<Self> + DivAssign<Self>  + From<f64>+ SubAssign<Self> + 'static {}

impl Scalar for f64 {}
impl Scalar for c128 {}

/// Allows a value to track some accumulation statistics
#[derive(Clone, Copy, Debug)]
pub struct AccumulationStats {
    /// size of accumulation
    pub n: f64,
}

pub trait Accumulation{
    fn set_accumulation_stats(&self, stats: AccumulationStats) -> Self;
    
    fn get_accumulation_stats(&self) -> AccumulationStats;
}
