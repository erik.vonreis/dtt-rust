pub (crate) mod types;
pub (crate) mod conditioning;
pub (crate) mod fourier_tools;
pub (crate) mod result;
pub (crate) mod general;
pub (crate) mod custom;
pub (crate) mod graph;
pub (crate) mod scope;
