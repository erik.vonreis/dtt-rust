//! Heterodyne, or downconverting, or Mixdown,
//! is the process of multiplying a signal by a complex sine wave
//! of a certain frequency
//! this has the effect of shifting signal components down in frequency, centered around
//! the frequency of the sine wave, so that frequencies below the sine wave frequency end up
//! as low-amplitude negative frequencies, and frequencies above the sine wave frequency end up
//! as low-amplitude positive frequencies.  this allows capturing of a
//! narrow band with a high center frequency at nyquist frequency that depends on the band width
//! rather than the upper frequency of the band.
//!
//! Because the sine wave is complex, cos (2 pi f) + i * sin(2 pi f), the output is always complex.
//!
//! This pipeline takes a real-valued signal and mixes it down into a complex signal.

use std::sync::Arc;
use futures::FutureExt;
use pipeline_macros::box_async;
use pipelines::complex::c128;
use pipelines::{PipelineSubscriber, PipeResult};
use pipelines::stateless::Stateless1;
use user_messages::UserMsgProvider;
use crate::analysis::types::time_domain_array::TimeDomainArray;
use gps_pip::{PipDuration, PipInstant};
use crate::AccumulationStats;
use crate::gds_sigp::heterodyne::mix_down;

#[derive(Clone, Debug)]
pub struct Heterodyne {
    mix_freq_hz: f64,
    start_time_pip: PipInstant,
    time_offset_pip: PipDuration,
    sample_period_pip: PipDuration,
}

impl Heterodyne {

    /// Offset calculation references process() in cds-crtools channelinput.cc
    /// ### References
    /// 1. cds-crtools channelinput.cc process()
    ///    https://git.ligo.org/cds/software/dtt/-/blob/4.1.1/src/dtt/storage/channelinput.cc#L669
    #[box_async]
    fn generate(_rc: Box<dyn UserMsgProvider>, config: &Heterodyne, input: Arc<TimeDomainArray<f64>>) -> PipeResult<TimeDomainArray<c128>> {
        let offset_pip: PipDuration = input.start_gps_pip - config.time_offset_pip - config.start_time_pip;
        let y = mix_down(
            input.data.as_slice(),
            config.sample_period_pip.to_seconds(),
            offset_pip.to_seconds(),
            config.mix_freq_hz
        );
        TimeDomainArray {
            start_gps_pip: input.start_gps_pip,
            rate_hz: input.rate_hz,
            data: y,
            accumulation_stats: AccumulationStats{n: 1.0},
        }.into()
    }

    pub async fn create(rc: Box<dyn UserMsgProvider>, name: impl Into<String>,
                  mix_freq_hz: f64, start_time_pip: PipInstant,
                  time_offset_pip: PipDuration, sample_period_pip: PipDuration,
                  input: &PipelineSubscriber<TimeDomainArray<f64>>) -> PipelineSubscriber<TimeDomainArray<c128>> {
        let config = Heterodyne {
            mix_freq_hz,
            start_time_pip,
            time_offset_pip,
            sample_period_pip,
        };

        Stateless1::create(rc, name.into(), Heterodyne::generate, config, input).await

    }
}