# DTT
A frequency-domain analysis tool for LIGO data.

# C library dependencies
This library depends on libgds-sigp for some math functions 
and libcds for some time functions.

# Building as python module.

From the command line, build with 

```aiignore
maturin develop
```

