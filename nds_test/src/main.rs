use std::collections::HashMap;
use std::ffi::CString;
use eframe::Frame;
use egui::{Color32, Context};
use gps_pip::{PipDuration, PipInstant};
use pyo3::Python;
use pyo3::types::PyModule;
use tokio::runtime::Runtime;
use dtt::{Accumulation, AnalysisResult};
use dtt::data_source::nds_cache::{DataFlow, NDS2Cache};
use dtt::params::{
    channel_params::{
        Channel,
        ChannelParams,
        NDSDataType,
    },
    test_params::{
        AverageType,
        FFTWindow, StartTime, TestParams
    },
};
use dtt::params::custom_pipeline::CustomPipeline;
use dtt::user::{ResponseToUser, UserOutputReceiver, DTT};
use user_messages::MessageJob;
use std::fs;
use dtt::ResultValue;

fn main() {
    let rt = Runtime::new().unwrap();
    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default().with_inner_size([1500.0,1000.0]),
        ..Default::default()
    };



    eframe::run_native(
        "dtt demo",
        options,
        Box::new(|_cc| Ok(Box::<Demo>::new(Demo::new(rt)))),
    ).unwrap();
}



struct Demo {
    _runtime: Runtime,
    latest: HashMap<String, dtt::FreqDomainValue<f64>>,
    test_params: TestParams,
    colors: HashMap<String, Color32>,
    dtt: DTT,
    keys: Vec<String>,
    display_rx: ResultsReceiver,
    msg_rx: MessageReceiver,
    use_python_code: bool,
    custom_file: String,
    _python_code: String,
    set_messages: HashMap<String, user_messages::UserMessage>,
    tp_loaded: bool,
}

// enum FlaggedAnalysisResult {
//     Customized(AnalysisResult),
//     Raw(AnalysisResult),
// }

type ResultsSender = tokio::sync::mpsc::Sender<HashMap<String, AnalysisResult>>;
type ResultsReceiver = tokio::sync::mpsc::Receiver<HashMap<String, AnalysisResult>>;

type MessageSender = tokio::sync::mpsc::Sender<MessageJob>;
type MessageReceiver = tokio::sync::mpsc::Receiver<MessageJob>;


const DEMO_CODE: &str = "def dtt_generate(input):
    new_data = []
    for i in range(len(input[2])):
        new_data.append( input[2][i] * 1/(i+1))
    #return (input[0], input[1], new_data)
    return input";

impl Demo {
    pub fn new(runtime: Runtime) -> Self {
        let (uc, out_rx) = dtt::init(runtime.handle()).unwrap();

        let (display_tx, display_rx) = tokio::sync::mpsc::channel(4);
        let (msg_tx, msg_rx) = tokio::sync::mpsc::channel(4);

        runtime.spawn(
          Self::read_dtt_custom(out_rx, display_tx, msg_tx)
        );

        let meas_chans = vec![ChannelParams {
                active: true,
                channel: Channel {
                    channel_name: "H1:GDS-CALIB_STRAIN".to_string(),
                    data_type: NDSDataType::Float64,
                    rate_hz: 16384.0,
                    ..Channel::default()
                },
            },
              ChannelParams {
                  active: true,
                  channel: Channel {
                      channel_name: "L1:GDS-CALIB_STRAIN".to_string(),
                      data_type: NDSDataType::Float64,
                      rate_hz: 16384.0,
                      ..Channel::default()
                  },
              },
        ];

        let mut colors = HashMap::new();
        colors.insert("H1:GDS-CALIB_STRAIN".to_string(), Color32::RED);
        colors.insert("L1:GDS-CALIB_STRAIN".into(), Color32::LIGHT_BLUE);

        let keys = vec!["H1:GDS-CALIB_STRAIN".into(), "L1:GDS-CALIB_STRAIN".into()];

        Self {
            _runtime: runtime,
            dtt: uc,
            latest: HashMap::new(),
            colors,
            keys,
            display_rx,
            msg_rx,
            _python_code: DEMO_CODE.into(),
            use_python_code: true,
            custom_file: "custom.py".into(),
            set_messages: HashMap::new(),
            tp_loaded: false,
            test_params:  TestParams {
                measurement_param_list: meas_chans,
                fft_window: FFTWindow::Hann,
                overlap: 0.5,
                band_width_hz: 1.0,
                average_type: AverageType::Fixed,
                average_size: 10,
                start_hz: 0.0,
                stop_hz: 900.0,
                ramp_down_pip: PipDuration::from_sec(1),
                settling_time_frac: 0.10,
                start_time_pip: StartTime::Bound(PipInstant::from_gpst_sec(1403722629u64)),
                ..TestParams::default_fft_params()
            },
        }
    }

    async fn read_dtt_custom(mut out_rx: UserOutputReceiver, display_tx: ResultsSender, msg_tx: MessageSender) {
        let mut last = HashMap::new();
        let mut count = 0;
        loop {
            match out_rx.recv().await {
                Some(ResponseToUser::NewResult(AnalysisResult::Custom{channels, pipeline_name, value})) => {

                    let a = AnalysisResult::Custom{
                        channels: channels.clone(), 
                        pipeline_name, value: value.clone()
                    };
                    last.insert(channels[0].channel_name.clone() + ".custom", a);
                    if count >= 250 {
                        if let Err(_) = display_tx.send(last.clone()).await {
                            break;
                        }
                        count = 0;
                    }
                    count += 1;

                },
                Some(ResponseToUser::NewResult(AnalysisResult::ASD{channel, value})) => {
                    let a = AnalysisResult::ASD{channel: channel.clone(),value};
                    last.insert(channel.channel_name, a.clone());
                    if count >= 250 {
                        if let Err(_) = display_tx.send(last.clone()).await {
                            break;
                        }
                        count = 0;
                    }
                    count += 1;
                },
                Some(ResponseToUser::UpdateMessages(m)) => {
                    if let Err(_) = msg_tx.send(m).await {
                        break;
                    }
                }
                Some(ResponseToUser::FinalResults(_)) => {
                    let _ = display_tx.send(last.clone()).await;
                    last.clear();
                },
                None => {
                    break;
                },
                _ => (),
            };
        }
    }

    fn read_results(&mut self) {
        match self.display_rx.try_recv() {
            Ok(results) => {
                for (_, value) in results {
                    match value {
                        AnalysisResult::ASD{channel, value} => {
                            if !self.use_python_code {
                                self.latest.insert(channel.channel_name, value);
                                dbg!("data");
                            }
                        },
                        AnalysisResult::Custom{channels, pipeline_name, value} => {
                            if self.use_python_code {
                                if let ResultValue::FreqDomainValueReal(f) = value {
                                    //self.latest.insert(c[0].channel_name[0..(c[0].channel_name.len() - 7)].to_string(), f);
                                    self.latest.insert(channels[0].channel_name.clone(), f);
                                }
                            }
                        }
                        _ => (),
                    }
                }

            },
            _ => (),
        }
    }

    fn read_msg(&mut self) {
        loop {
            match self.msg_rx.try_recv() {
                Ok(msg) => {
                    match msg {
                        MessageJob::SetMessage(tag, um) => {
                            self.set_messages.insert(tag, um);
                        }
                        MessageJob::ClearMessage(tag) => {
                            self.set_messages.remove(&tag);
                        }
                    }
                },
                _ => break,
            }
        }
    }

    fn get_results_curve(&self) -> HashMap<String, egui_plot::Line> {
        let mut plots = HashMap::new();
        for (key, result) in &self.latest {
            let dtt::FreqDomainValue::FixedStepArray(f) = result;
            let line = egui_plot::Line::new((0..f.data.len())
                .map(|i| { [i as f64 * f.bucket_width_hz + f.start_hz, f.data[i].log10()] })
                .collect::<egui_plot::PlotPoints>())
                .color(self.colors.get(key).unwrap().clone());
            plots.insert(key.clone(), line);
        }
        plots
    }

    fn set_test_params(&mut self) -> Result<(), String> {
        let mut test_params = self.test_params.clone();

        if self.use_python_code {
            let code = fs::read_to_string(self.custom_file.clone()).map_err(|x| x.to_string())?;
            
            let py_module = Python::with_gil(|py| {
                let res = PyModule::from_code(
                    py,
                    CString::new(code).unwrap().as_c_str(),
                    CString::new("").unwrap().as_c_str(),
                    CString::new("").unwrap().as_c_str(),
                );

                let bound = match res {
                    Ok(r) => r,
                    Err(e) => return Err(e),
                };

                Ok(bound.unbind())
            }
            ).map_err(|e| "Failed to create python module: ".to_string() +  e.to_string().as_str())?;

            let cp = CustomPipeline::new(
                "custom1",
                &["asd_avg".to_string()],
                       // &["custom1".to_string()],
                py_module);

            test_params.custom_pipelines = vec![cp];
        }
        else {
            test_params.custom_pipelines = Vec::new();
        }



        self.dtt.set_test_params(test_params).unwrap();
        self.tp_loaded = true;
        Ok(())
    }

    fn start_test(&mut self) {

        let data_source =
            //NDS2Direct::new("nds.ligo.caltech.edu", 31200);
            NDS2Cache::new(1<<30, "/tmp/ndscache".into());

        self.dtt.set_data_source(data_source.into()).unwrap();
        self.dtt.run_test().unwrap();
    }

    fn compose_set_messages(& self) -> String {
        self.set_messages.values()
            .map(|m| m.message.clone())
            .reduce(|a, b| {a + "\n" + b.as_str()}).unwrap_or(String::new())
    }
}
   

impl eframe::App for Demo {
    fn update(&mut self, ctx: &Context, _frame: &mut Frame) {

        self.read_results();
        self.read_msg();

        let start_time_s = match &self.test_params.start_time_pip {
            StartTime::Bound(s) => s.to_gpst_seconds() as u64,
            StartTime::Unbound() => 0,
        };

        let mut start_string = start_time_s.to_string();
        let mut avg_string = self.test_params.average_size.to_string();
        let mut set_msg = self.compose_set_messages();

        egui::SidePanel::left("control").show(ctx, |ui| {
            if ui.button("Run Test").clicked() {
                self.set_test_params().unwrap();
                self.latest.clear();
                self.start_test();
            }
            ui.label("start GPS");
            if ui.text_edit_singleline(&mut start_string).changed() {
                let new_start_time_s: u64 = start_string.parse().unwrap_or(start_time_s);
                self.test_params.start_time_pip = if new_start_time_s > 0 {
                    StartTime::Bound(PipInstant::from_gpst_sec(new_start_time_s))
                } else { StartTime::Unbound() };
                self.tp_loaded = false;
            }
            ui.label("averages");
            if ui.text_edit_singleline(&mut avg_string).changed() {
                self.test_params.average_size = avg_string.parse().unwrap_or(self.test_params.average_size);
                self.tp_loaded = false;
            }

            let count1 = match self.latest.get("H1:GDS-CALIB_STRAIN") {
                Some(f) => f.get_accumulation_stats().n,
                None => 0.0,
            };
            let count2 = match self.latest.get("L1:GDS-CALIB_STRAIN") {
                Some(f) => f.get_accumulation_stats().n,
                None => 0.0,
            };

            let count_label = format!("count: {} {}", count1 as i32, count2 as i32);
            ui.label(count_label);

            // single input python custom pipeline
            if ui.checkbox(&mut self.use_python_code, "use python code").changed() {
                self.tp_loaded = false;
            };
            // if ui.text_edit_multiline(&mut self.python_code).changed() {
            //     self.tp_loaded = false;
            // };
            ui.label("custom pipeline script");
            if ui.text_edit_singleline(&mut self.custom_file).changed() {
                self.tp_loaded = false;
            }
            
            ui.label("messages");
            ui.text_edit_multiline(&mut set_msg).changed();
            
        });

        egui::CentralPanel::default().show(ctx, |ui|{

            let mut lines = self.get_results_curve();

            egui_plot::Plot::new("plot").show(ui, |plot_ui|{
                for key in &self.keys {
                    if lines.contains_key(key) {
                        plot_ui.line(lines.remove(key).unwrap());
                    }
                }
            });
        });

        ctx.request_repaint();

        if ! self.tp_loaded {
            let _ = self.set_test_params();
        }
    }

    fn on_exit(&mut self, _gl: Option<&eframe::glow::Context>) {
        //dtt::shutdown(& mut self.uc);
    }
}